# Overview
An EEM power injector to allow for independant powering of [Sinara](https://github.com/sinara-hw) EEM hardware.
Apply 12V to the Anderson Connectors. 
Optional Resistor R1 can be populated to short the incoming and outgoing grounds.


